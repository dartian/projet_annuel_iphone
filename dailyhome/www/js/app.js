// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var dailyhome = angular.module('dailyhome', ['ionic']);

dailyhome.config(function($stateProvider,$urlRouterProvider){
  $stateProvider
    .state("logIn", {
      url:"/",
      templateUrl:"template/logIn.html",
      controller:"connexion"
    })
    .state("home", {
      url:"/home",
      templateUrl:"template/home.html",
      controller:"home"
  })
    .state("signIn", {
      url:"/signIn",
      templateUrl:"template/signIn.html",
      controller:"signIn"
    })
    .state("account", {
      url:"/account",
      templateUrl:"template/account.html",
      controller:"account"
  })
    .state("history", {
      url:"/history",
      templateUrl:"template/history.html",
      controller:"history"
    }).state("mission", {
      url:"/mission/:id",
      templateUrl:"template/mission.html",
      controller:"mission"
  })
    .state("add", {
      url:"/add",
      templateUrl:"template/create.html",
      controller:"create"
    });
  $urlRouterProvider.otherwise("/");
});

dailyhome.run(function($ionicPlatform) {
  $ionicPlatform.ready(function () {
    if (window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  })
});

dailyhome.controller("connexion", ["$http", "$scope","$state","$ionicPopup", function($http, $scope, $state, $ionicPopup){
  //$state.go("home");
  $scope.user = {};
  this.username = "";
  this.password = "";
  $scope.connexion = function(){
    if(this.username===""&& this.password===""){
      $ionicPopup.show({
        title: "Nom d'utilisateur ou de mots de passe incorect",
        scope: $scope,
        buttons: [
          {
            text: '<b>Valider</b>'
          }
        ]
      });
    }else{
      $state.go("home");
    }
  };
}]);

dailyhome.controller("home", ["$http", "$scope", function($http, $scope){
  $scope.missions = [{name:"test", text:"test", id:1}]
}]);

dailyhome.controller("account", ["$http", "$scope", function($http, $scope){

}]);

dailyhome.controller("history", ["$http", "$scope", function($http, $scope){
  $scope.histories = [];
}]);

dailyhome.controller("mission", ["$scope", function($scope){
}]);

dailyhome.controller("signIn", ["$scope", function($scope){
  var date= new Date();
  $scope.dateMax = date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();
}]);

dailyhome.controller("create", ["$http", "$scope","$ionicPopup", function($http, $scope, $ionicPopup){


  this.address = "";
  this.ville = "";
  this.cp= 0;
  this.activityType = 0;
  this.date = "";
  this.time = "";
  this.duree = 0;
  this.addressChoice = 0;
  this.particularity="";

  $scope.displayPopup = function(){

    var myPopup = $ionicPopup.show({
      templateUrl: 'template/addressPopup.html',
      title: 'Entrer une nouvelle adresse',
      scope: $scope,
      buttons: [
        { text: 'Annuler' },
        {
          text: '<b>Enregistrer</b>',
          type: 'button-positive',
          onTap: function(e) {
            if (this.address==="" && this.ville==="" && this.cp==="") {
              //don't allow the user to close unless he enters wifi password
              e.preventDefault();
            } else {
              return this.address +" "+ this.ville + "" + this.cp;
            }
          }
        }
      ]
    });
  };
}]);

